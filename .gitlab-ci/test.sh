#!/bin/bash

for d in *; do echo >> $d/builder.yml; done
for d in *; do echo >> $d/builder.yml; done
for d in *; do echo "test_spigot_dev_${d}:" >> $d/builder.yml; done
for d in *; do echo "  stage: scan" >> $d/builder.yml; done
for d in *; do echo "  image: docker:stable" >> $d/builder.yml; done
for d in *; do echo "  needs: [spigot_dev_${d}]" >> $d/builder.yml; done
for d in *; do echo "  services:" >> $d/builder.yml; done
for d in *; do echo "    - docker:dind" >> $d/builder.yml; done
for d in *; do echo "  variables:" >> $d/builder.yml; done
for d in *; do echo "    DOCKER_DRIVER: overlay2" >> $d/builder.yml; done
for d in *; do echo "  allow_failure: true" >> $d/builder.yml; done
for d in *; do echo "  before_script:" >> $d/builder.yml; done
for d in *; do echo "    - docker login -u \"\$CI_REGISTRY_USER\" -p \"\$CI_REGISTRY_PASSWORD\" \$CI_REGISTRY" >> $d/builder.yml; done
for d in *; do echo "  script:" >> $d/builder.yml; done
for d in *; do echo "    - docker run -e EULA=true -e BUILDTEST=true \${CI_REGISTRY_IMAGE}:\${CI_COMMIT_REF_SLUG}-spigot-${d}" >> $d/builder.yml; done
for d in *; do echo "  except:" >> $d/builder.yml; done
for d in *; do echo "    - master" >> $d/builder.yml; done

for d in *; do echo >> $d/builder.yml; done
for d in *; do echo "test_spigot_${d}:" >> $d/builder.yml; done
for d in *; do echo "  stage: scan" >> $d/builder.yml; done
for d in *; do echo "  image: docker:stable" >> $d/builder.yml; done
for d in *; do echo "  needs: [spigot_${d}]" >> $d/builder.yml; done
for d in *; do echo "  services:" >> $d/builder.yml; done
for d in *; do echo "    - docker:dind" >> $d/builder.yml; done
for d in *; do echo "  variables:" >> $d/builder.yml; done
for d in *; do echo "    DOCKER_DRIVER: overlay2" >> $d/builder.yml; done
for d in *; do echo "  allow_failure: true" >> $d/builder.yml; done
for d in *; do echo "  before_script:" >> $d/builder.yml; done
for d in *; do echo "    - docker login -u \"\$CI_REGISTRY_USER\" -p \"\$CI_REGISTRY_PASSWORD\" \$CI_REGISTRY" >> $d/builder.yml; done
for d in *; do echo "  script:" >> $d/builder.yml; done
for d in *; do echo "    - CI_REGISTRY_IMAGE=index.docker.io/cmunroe/spigot" >> $d/builder.yml; done
for d in *; do echo "    - docker run -e EULA=true -e BUILDTEST=true \${CI_REGISTRY_IMAGE}:${d}" >> $d/builder.yml; done
for d in *; do echo "  only:" >> $d/builder.yml; done
for d in *; do echo "    - master" >> $d/builder.yml; done

# Bukkit
for d in *; do echo >> $d/builder.yml; done
for d in *; do echo "test_spigot_dev_${d}:" >> $d/builder.yml; done
for d in *; do echo "  stage: scan" >> $d/builder.yml; done
for d in *; do echo "  image: docker:stable" >> $d/builder.yml; done
for d in *; do echo "  needs: [bukkit_dev_${d}]" >> $d/builder.yml; done
for d in *; do echo "  services:" >> $d/builder.yml; done
for d in *; do echo "    - docker:dind" >> $d/builder.yml; done
for d in *; do echo "  variables:" >> $d/builder.yml; done
for d in *; do echo "    DOCKER_DRIVER: overlay2" >> $d/builder.yml; done
for d in *; do echo "  allow_failure: true" >> $d/builder.yml; done
for d in *; do echo "  before_script:" >> $d/builder.yml; done
for d in *; do echo "    - docker login -u \"\$CI_REGISTRY_USER\" -p \"\$CI_REGISTRY_PASSWORD\" \$CI_REGISTRY" >> $d/builder.yml; done
for d in *; do echo "  script:" >> $d/builder.yml; done
for d in *; do echo "    - docker run -e EULA=true -e BUILDTEST=true \${CI_REGISTRY_IMAGE}:\${CI_COMMIT_REF_SLUG}-${d}" >> $d/builder.yml; done
for d in *; do echo "  except:" >> $d/builder.yml; done
for d in *; do echo "    - master" >> $d/builder.yml; done

for d in *; do echo >> $d/builder.yml; done
for d in *; do echo "test_bukkit_${d}:" >> $d/builder.yml; done
for d in *; do echo "  stage: scan" >> $d/builder.yml; done
for d in *; do echo "  image: docker:stable" >> $d/builder.yml; done
for d in *; do echo "  needs: [bukkit_${d}]" >> $d/builder.yml; done
for d in *; do echo "  services:" >> $d/builder.yml; done
for d in *; do echo "    - docker:dind" >> $d/builder.yml; done
for d in *; do echo "  variables:" >> $d/builder.yml; done
for d in *; do echo "    DOCKER_DRIVER: overlay2" >> $d/builder.yml; done
for d in *; do echo "  allow_failure: true" >> $d/builder.yml; done
for d in *; do echo "  before_script:" >> $d/builder.yml; done
for d in *; do echo "    - docker login -u \"\$CI_REGISTRY_USER\" -p \"\$CI_REGISTRY_PASSWORD\" \$CI_REGISTRY" >> $d/builder.yml; done
for d in *; do echo "  script:" >> $d/builder.yml; done
for d in *; do echo "    - docker run -e EULA=true -e BUILDTEST=true \${CI_REGISTRY_IMAGE}:${d}" >> $d/builder.yml; done
for d in *; do echo "  only:" >> $d/builder.yml; done
for d in *; do echo "    - master" >> $d/builder.yml; done